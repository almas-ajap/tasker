using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Tasker.Data.Entities;
using Tasker.Data.Models;
using Microsoft.Extensions.Configuration;


namespace Tasker.Services
{
    public class TokenService
    {
        private readonly IConfiguration configuration;
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;

        public TokenService(IConfiguration configuration, JwtSecurityTokenHandler jwtSecurityTokenHandler)
        {
            this.configuration = configuration;
            this.jwtSecurityTokenHandler = jwtSecurityTokenHandler;
        }

        public string GetValidToken(string name, string role, double hours) => jwtSecurityTokenHandler.WriteToken(jwtSecurityTokenHandler.CreateToken(new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimTypes.Role, role)
                }),
            Expires = DateTime.UtcNow.AddHours(hours),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(this.configuration["TokenSecret"])), SecurityAlgorithms.HmacSha256Signature)
        }));

    }
}