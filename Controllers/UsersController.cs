using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Tasker.Data;
using Tasker.Data.Models;
using Tasker.Data.Repos;
using Tasker.Services;

namespace Tasker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepo repo;

        public UsersController(IUserRepo repo)
        {
            this.repo = repo;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(this.repo.GetAllUsers());
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public IActionResult Add([FromBody] UserWriteModel userWriteModel)
        {
            try
            {
                return Ok(this.repo.AddUser(userWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(this.repo.GetUserById(id));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("{id}")]
        public IActionResult Update(int id, [FromBody] UserWriteModel userWriteModel)
        {
            try
            {
                return Ok(this.repo.UpdateUserById(id, userWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.repo.DeleteUserById(id);
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
