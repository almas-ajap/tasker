using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Tasker.Data;
using Tasker.Data.Models;
using Tasker.Data.Repos;
using Tasker.Services;

namespace Tasker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatesController : ControllerBase
    {
        private readonly IStateRepo repo;

        public StatesController(IStateRepo repo)
        {
            this.repo = repo;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(this.repo.GetAllStates());
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public IActionResult Add([FromBody] StateWriteModel stateWriteModel)
        {
            try
            {
                return Ok(this.repo.AddState(stateWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(this.repo.GetStateById(id));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("{id}")]
        public IActionResult Update(int id, [FromBody] StateWriteModel stateWriteModel)
        {
            try
            {
                return Ok(this.repo.UpdateStateById(id, stateWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.repo.DeleteStateById(id);
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
