using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Tasker.Data;
using Tasker.Data.Models;
using Tasker.Data.Repos;
using Tasker.Services;

namespace Tasker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ToDosController : ControllerBase
    {
        private readonly IToDoRepo repo;

        public ToDosController(IToDoRepo repo)
        {
            this.repo = repo;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(this.repo.GetAllToDos());
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public IActionResult Add([FromBody] ToDoWriteModel toDoWriteModel)
        {
            try
            {
                return Ok(this.repo.AddToDo(toDoWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(this.repo.GetToDoById(id));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("{id}")]
        public IActionResult Update(int id, [FromBody] ToDoWriteModel toDoWriteModel)
        {
            try
            {
                return Ok(this.repo.UpdateToDoById(id, toDoWriteModel));
            }
            catch
            {
                return StatusCode(500);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.repo.DeleteToDoById(id);
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
