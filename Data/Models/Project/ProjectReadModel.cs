using System;
using System.Collections.Generic;

namespace Tasker.Data.Models
{
    public class ProjectReadModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public UserReadModel Admin { get; set; }

        public IEnumerable<ToDoReadModel> ToDos { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}