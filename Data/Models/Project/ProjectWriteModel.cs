using System;
using System.ComponentModel.DataAnnotations;

namespace Tasker.Data.Models
{
    public class ProjectWriteModel
    {
        [Required(ErrorMessage = "required")]
        [RegularExpression("^.+$", ErrorMessage = "one or more chars")]
        public string Name { get; set; }

        [Required(ErrorMessage = "required")]
        [RegularExpression("^.+$", ErrorMessage = "one or more chars")]
        public string Description { get; set; }

        public int AdminId { get; set; }
    }
}