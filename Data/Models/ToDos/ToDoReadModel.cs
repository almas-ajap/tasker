using System;
using System.Collections.Generic;

namespace Tasker.Data.Models
{
    public class ToDoReadModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public UserReadModel User { get; set; }

        public ProjectReadModel Project { get; set; }

        public IEnumerable<StateReadModel> States { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime StartAt { get; set; }
    }
}