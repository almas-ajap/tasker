using System;
using System.ComponentModel.DataAnnotations;

namespace Tasker.Data.Models
{
    public class ToDoWriteModel
    {
        [Required(ErrorMessage = "required")]
        [RegularExpression("^.+$", ErrorMessage = "one or more chars")]
        public string Name { get; set; }

        [Required(ErrorMessage = "required")]
        [RegularExpression("^.+$", ErrorMessage = "one or more chars")]
        public string Description { get; set; }

        public int ProjectId { get; set; }

        public int UserId { get; set; }

        public DateTime StartAt { get; set; }
    }
}