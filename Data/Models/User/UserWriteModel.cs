using System.ComponentModel.DataAnnotations;

namespace Tasker.Data.Models
{
    public class UserWriteModel
    {
        [Required(ErrorMessage="required")]
        [EmailAddress(ErrorMessage = "valid email format")]
        public string Email { get; set; }

        [Required(ErrorMessage="required")]
        [RegularExpression("(?=^.{8,}$)(?=.*\\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = "at least: 8 chars, including at least: 1 uppercase, 1 lowercase, 1 special char")]
        public string Password { get; set; }

        [Required(ErrorMessage="required")]
        [RegularExpression("^Admin|User$", ErrorMessage = "availible roles are: Admin, User")]
        public string Role { get; set; }
    }
}