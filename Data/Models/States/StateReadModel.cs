using System;

namespace Tasker.Data.Models
{
    public class StateReadModel
    {
        public int Id { get; set; }

        public string Info { get; set; }

        public ToDoReadModel ToDo {get;set;}

        public DateTime CreatedAt { get; set; }
    }
}