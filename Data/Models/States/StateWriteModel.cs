using System;
using System.ComponentModel.DataAnnotations;

namespace Tasker.Data.Models
{
    public class StateWriteModel
    {
        [Required(ErrorMessage = "required")]
        [RegularExpression("^Working|Complete$", ErrorMessage = "availible states are: Working, Complete")]
        public string Info { get; set; }

        public int ToDoId { get; set; }
    }
}