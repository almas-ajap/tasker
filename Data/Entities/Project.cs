using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Tasker.Data.Entities
{
    public class Project
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int AdminId { get; set; }

        public DateTime CreatedAt {get;set;}

        public DateTime UpdatedAt {get;set;}
    }
}