using System;

namespace Tasker.Data.Entities
{
    public class State
    {
        public int Id { get; set; }

        public int ToDoId { get; set; }

        public string Info { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}