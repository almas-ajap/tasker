using Microsoft.EntityFrameworkCore;
using Tasker.Data.Entities;
using Tasker.Data.Models;

namespace Tasker.Data
{
    public class TaskerContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<ToDo> ToDos { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<State> States { get; set; }

        public TaskerContext(DbContextOptions<TaskerContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}