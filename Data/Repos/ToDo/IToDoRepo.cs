using System.Collections.Generic;
using System.Threading.Tasks;
using Tasker.Data.Models;

namespace Tasker.Data.Repos
{
    public interface IToDoRepo
    {
        public ToDoReadModel GetToDoById(int Id);

        public IEnumerable<ToDoReadModel> GetAllToDos();

        public ToDoReadModel AddToDo(ToDoWriteModel userWriteModel);

        public ToDoReadModel UpdateToDoById(int id, ToDoWriteModel userWriteModel);

        public void DeleteToDoById(int id);
    }
}