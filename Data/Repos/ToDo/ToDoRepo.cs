using Tasker.Data.Entities;
using Tasker.Data.Models;
using AutoMapper;
using System.Linq;
using System;
using AutoMapper.Configuration;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Tasker.Data.Repos
{
    public class ToDoRepo : IToDoRepo
    {
        private readonly TaskerContext dbContext;
        private readonly IMapper mapper;

        private IEnumerable<ToDoReadModel> GetToDoReadModels()
        {
            return from toDo in this.dbContext.ToDos.ToArray()
                   join state in this.dbContext.States.ToArray() on toDo.Id equals state.ToDoId into states
                   join project in this.dbContext.Projects.ToArray() on toDo.ProjectId equals project.Id into matchProjects
                   from matchProject in matchProjects.DefaultIfEmpty(null)
                   join user in this.dbContext.Users.ToArray() on toDo.UserId equals user.Id into matchUsers
                   from matchUser in matchUsers.DefaultIfEmpty(null)
                   select new ToDoReadModel()
                   {
                       Id = toDo.Id,
                       Name = toDo.Name,
                       Description = toDo.Description,
                       User = mapper.Map<UserReadModel>(matchUser),
                       CreatedAt = toDo.CreatedAt,
                       UpdatedAt = toDo.UpdatedAt,
                       StartAt = toDo.StartAt,
                       Project = mapper.Map<ProjectReadModel>(matchProject),
                       States = mapper.Map<IEnumerable<StateReadModel>>(states)
                   };
        }

        public ToDoRepo(TaskerContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public IEnumerable<ToDoReadModel> GetAllToDos()
        {
            return GetToDoReadModels();
        }

        public ToDoReadModel GetToDoById(int id)
        {
            return GetToDoReadModels().Single(m => m.Id == id);
        }

        public ToDoReadModel AddToDo(ToDoWriteModel projectWriteModel)
        {
            ToDo p = this.mapper.Map<ToDo>(projectWriteModel);
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = p.CreatedAt;
            this.dbContext.Add(p);
            this.dbContext.SaveChanges();
            ToDoReadModel trm = GetToDoReadModels().Single(m => m.Id == p.Id);

            return trm;
        }

        public ToDoReadModel UpdateToDoById(int id, ToDoWriteModel projectWriteModel)
        {
            ToDo mappedToDo = mapper.Map<ToDo>(projectWriteModel);
            mappedToDo.Id = id;
            mappedToDo.UpdatedAt = DateTime.Now;
            mappedToDo.CreatedAt = dbContext.ToDos.AsNoTracking().Single(p => p.Id == id).CreatedAt;
            dbContext.ToDos.Update(mappedToDo);
            dbContext.SaveChanges();
            return GetToDoReadModels().Single(m => m.Id == mappedToDo.Id);
        }

        public void DeleteToDoById(int id)
        {
            dbContext.ToDos.Remove(dbContext.ToDos.Single(p => p.Id == id));
            dbContext.SaveChanges();
        }
    }
}