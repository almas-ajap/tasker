using Tasker.Data.Entities;
using Tasker.Data.Models;
using AutoMapper;
using System.Linq;
using System;
using AutoMapper.Configuration;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Tasker.Data.Repos
{
    public class UserRepo : IUserRepo
    {
        private readonly TaskerContext dbContext;
        private readonly IMapper mapper;

        public UserRepo(TaskerContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public IEnumerable<UserReadModel> GetAllUsers()
        {
            return mapper.Map<IEnumerable<UserReadModel>>(this.dbContext.Users.ToArray());
        }

        public UserReadModel GetUserById(int id)
        {
            return mapper.Map<UserReadModel>(this.dbContext.Users.Single(u => u.Id == id));
        }

        public UserReadModel AddUser(UserWriteModel userWriteModel)
        {
            User u = this.mapper.Map<User>(userWriteModel);
            u.CreatedAt = DateTime.Now;
            u.UpdatedAt = u.CreatedAt;
            this.dbContext.Add(u);
            this.dbContext.SaveChanges();
            return this.mapper.Map<UserReadModel>(u);
        }

        public UserReadModel UpdateUserById(int id, UserWriteModel userWriteModel)
        {
            User mappedUser = mapper.Map<User>(userWriteModel);
            mappedUser.Id = id;
            mappedUser.UpdatedAt = DateTime.Now;
            mappedUser.CreatedAt = dbContext.Users.AsNoTracking().Single(u => u.Id == id).CreatedAt;
            dbContext.Users.Update(mappedUser);
            dbContext.SaveChanges();
            return mapper.Map<UserReadModel>(mappedUser);
        }

        public UserReadModel GetUserByEmail(string email)
        {
            return mapper.Map<UserReadModel>(this.dbContext.Users.Single(u => u.Email == email));
        }

        public void DeleteUserById(int id)
        {
            dbContext.Users.Remove(dbContext.Users.Single(u => u.Id == id));
            dbContext.SaveChanges();
        }
    }
}