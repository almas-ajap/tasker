using System.Collections.Generic;
using System.Threading.Tasks;
using Tasker.Data.Models;

namespace Tasker.Data.Repos
{
    public interface IUserRepo
    {
        public UserReadModel GetUserById(int Id);

        public UserReadModel GetUserByEmail(string email);

        public IEnumerable<UserReadModel> GetAllUsers();

        public UserReadModel AddUser(UserWriteModel userWriteModel);

        public UserReadModel UpdateUserById(int id, UserWriteModel userWriteModel);

        public void DeleteUserById(int id);
    }
}