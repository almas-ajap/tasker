using Tasker.Data.Entities;
using Tasker.Data.Models;
using AutoMapper;
using System.Linq;
using System;
using AutoMapper.Configuration;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Tasker.Data.Repos
{
    public class StateRepo : IStateRepo
    {
        private readonly TaskerContext dbContext;
        private readonly IMapper mapper;

        private IEnumerable<StateReadModel> GetStateReadModels()
        {
            return from state in this.dbContext.States.ToArray()
                    join toDo in this.dbContext.ToDos.ToArray() on state.ToDoId equals toDo.Id into matchToDos
                    from matchToDo in matchToDos.DefaultIfEmpty(null)
                    select new StateReadModel()
                    {
                        Id = state.Id,
                        Info = state.Info,
                        ToDo = mapper.Map<ToDoReadModel>(matchToDo),
                    };
        }

        public StateRepo(TaskerContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public IEnumerable<StateReadModel> GetAllStates()
        {
            return GetStateReadModels();
        }

        public StateReadModel GetStateById(int id)
        {
            return GetStateReadModels().Single(m => m.Id == id);
        }

        public StateReadModel AddState(StateWriteModel projectWriteModel)
        {
            State p = this.mapper.Map<State>(projectWriteModel);
            p.CreatedAt = DateTime.Now;
            this.dbContext.Add(p);
            this.dbContext.SaveChanges();
            return GetStateReadModels().Single(m => m.Id == p.Id);
        }

        public StateReadModel UpdateStateById(int id, StateWriteModel projectWriteModel)
        {
            State mappedState = mapper.Map<State>(projectWriteModel);
            mappedState.Id = id;
            mappedState.CreatedAt = dbContext.States.AsNoTracking().Single(p => p.Id == id).CreatedAt;
            dbContext.States.Update(mappedState);
            dbContext.SaveChanges();
            return GetStateReadModels().Single(m => m.Id == mappedState.Id);
        }

        public void DeleteStateById(int id)
        {
            dbContext.States.Remove(dbContext.States.Single(p => p.Id == id));
            dbContext.SaveChanges();
        }
    }
}