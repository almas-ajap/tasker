using System.Collections.Generic;
using System.Threading.Tasks;
using Tasker.Data.Models;

namespace Tasker.Data.Repos
{
    public interface IStateRepo
    {
        public StateReadModel GetStateById(int Id);

        public IEnumerable<StateReadModel> GetAllStates();

        public StateReadModel AddState(StateWriteModel userWriteModel);

        public StateReadModel UpdateStateById(int id, StateWriteModel userWriteModel);

        public void DeleteStateById(int id);
    }
}