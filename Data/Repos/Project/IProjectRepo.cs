using System.Collections.Generic;
using System.Threading.Tasks;
using Tasker.Data.Models;

namespace Tasker.Data.Repos
{
    public interface IProjectRepo
    {
        public ProjectReadModel GetProjectById(int Id);

        public IEnumerable<ProjectReadModel> GetAllProjects();

        public ProjectReadModel AddProject(ProjectWriteModel userWriteModel);

        public ProjectReadModel UpdateProjectById(int id, ProjectWriteModel userWriteModel);

        public void DeleteProjectById(int id);
    }
}