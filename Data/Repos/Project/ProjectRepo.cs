using Tasker.Data.Entities;
using Tasker.Data.Models;
using AutoMapper;
using System.Linq;
using System;
using AutoMapper.Configuration;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Tasker.Data.Repos
{
    public class ProjectRepo : IProjectRepo
    {
        private readonly TaskerContext dbContext;
        private readonly IMapper mapper;

        private IEnumerable<ProjectReadModel> GetProjectReadModels()
        {
            return from project in this.dbContext.Projects.ToArray()
                    join toDo in this.dbContext.ToDos.ToArray() on project.Id equals toDo.ProjectId into toDos
                    join user in this.dbContext.Users.ToArray() on project.AdminId equals user.Id into matchUsers
                    from matchUser in matchUsers.DefaultIfEmpty(null)
                    select new ProjectReadModel()
                    {
                        Id = project.Id,
                        Name = project.Name,
                        Description = project.Description,
                        Admin = mapper.Map<UserReadModel>(matchUser),
                        CreatedAt = project.CreatedAt,
                        UpdatedAt = project.UpdatedAt,
                        ToDos = mapper.Map<IEnumerable<ToDoReadModel>>(toDos)
                    };
        }

        public ProjectRepo(TaskerContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public IEnumerable<ProjectReadModel> GetAllProjects()
        {
            return GetProjectReadModels();
        }

        public ProjectReadModel GetProjectById(int id)
        {
            return GetProjectReadModels().Single(m => m.Id == id);
        }

        public ProjectReadModel AddProject(ProjectWriteModel projectWriteModel)
        {
            Project p = this.mapper.Map<Project>(projectWriteModel);
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = p.CreatedAt;
            this.dbContext.Add(p);
            this.dbContext.SaveChanges();
            return GetProjectReadModels().Single(m => m.Id == p.Id);
        }

        public ProjectReadModel UpdateProjectById(int id, ProjectWriteModel projectWriteModel)
        {
            Project mappedProject = mapper.Map<Project>(projectWriteModel);
            mappedProject.Id = id;
            mappedProject.UpdatedAt = DateTime.Now;
            mappedProject.CreatedAt = dbContext.Projects.AsNoTracking().Single(p => p.Id == id).CreatedAt;
            dbContext.Projects.Update(mappedProject);
            dbContext.SaveChanges();
            return GetProjectReadModels().Single(m => m.Id == mappedProject.Id);
        }

        public void DeleteProjectById(int id)
        {
            dbContext.Projects.Remove(dbContext.Projects.Single(p => p.Id == id));
            dbContext.SaveChanges();
        }
    }
}