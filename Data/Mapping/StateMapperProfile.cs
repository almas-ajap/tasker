using System;
using AutoMapper;
using Tasker.Data.Entities;
using Tasker.Data.Models;

namespace Tasker.Mapping
{
    public class StateMapperProfile  : Profile
    {
        public StateMapperProfile ()
        {
            CreateMap<StateWriteModel, State>();
            CreateMap<State, StateReadModel>();
        }
    }
}