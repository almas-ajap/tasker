using System;
using AutoMapper;
using Tasker.Data.Entities;
using Tasker.Data.Models;

namespace Tasker.Mapping
{
    public class ProjectMapperProfile : Profile
    {
        public ProjectMapperProfile()
        {
            CreateMap<ProjectWriteModel, Project>();
            CreateMap<Project, ProjectReadModel>();
        }
    }
}