using System;
using AutoMapper;
using Tasker.Data.Entities;
using Tasker.Data.Models;

namespace Tasker.Mapping
{
    public class PasswordToHashedPasswordConverter : ITypeConverter<UserWriteModel, User>
    {
        public User Convert(UserWriteModel source, User destination, ResolutionContext context)
        {
            return new User()
            {
                Email = source.Email,
                Role = source.Role,
                HashedPassword = BCrypt.Net.BCrypt.HashPassword(source.Password),
            };
        }
    }

    public class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            CreateMap<UserWriteModel, User>().ConvertUsing(new PasswordToHashedPasswordConverter());
            CreateMap<User, UserReadModel>();
        }
    }
}