using System;
using AutoMapper;
using Tasker.Data.Entities;
using Tasker.Data.Models;

namespace Tasker.Mapping
{
    public class ToDoMapperProfile  : Profile
    {
        public ToDoMapperProfile ()
        {
            CreateMap<ToDoWriteModel, ToDo>();
            CreateMap<ToDo, ToDoReadModel>();
        }
    }
}