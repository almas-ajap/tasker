angular.module('tasker').component('projectsList', {
    templateUrl: 'shared/projectsList/projectsList.html',
    bindings: {
        projects: '<?',
        onDelete: '&'
    },
    controller: [
        function (
        ) {
            $ctrl = this;

            $ctrl.propertyName = 'updatedAt';
            $ctrl.reverse = true;

            $ctrl.sortBy = function (propertyName) {
                $ctrl.reverse = ($ctrl.propertyName === propertyName) ? !$ctrl.reverse : false;
                $ctrl.propertyName = propertyName;
            };
        }
    ]
});
