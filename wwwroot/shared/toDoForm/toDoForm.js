angular.module('tasker').component('toDoForm', {
    templateUrl: 'shared/toDoForm/toDoForm.html',
    bindings: {
        toDo: '<',
        users: '<',
        lastState: '<',
        projects: '<',
        errs: '<?',
        onSubmit: '&',
        onStateChange: '&'
    },
    controller: [
        'orderByFilter',
        function (
            orderBy
        ) {
            $ctrl = this;
            $ctrl.orderBy = orderBy;
        }
    ]
});
