angular.module('tasker').component('usersList', {
    templateUrl: 'shared/usersList/usersList.html',
    bindings: {
        users: '<?',
        onDelete: '&'
    },
    controller: [
        function (
        ) {
            $ctrl = this;

            $ctrl.propertyName = 'updatedAt';
            $ctrl.reverse = true;

            $ctrl.sortBy = function (propertyName) {
                $ctrl.reverse = ($ctrl.propertyName === propertyName) ? !$ctrl.reverse : false;
                $ctrl.propertyName = propertyName;
            };
        }
    ]
});
