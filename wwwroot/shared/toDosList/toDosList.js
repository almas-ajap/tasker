angular.module('tasker').component('toDosList', {
    templateUrl: 'shared/toDosList/toDosList.html',
    bindings: {
        toDos: '<?',
        onDelete: '&'
    },
    controller: [
        function (
        ) {
            $ctrl = this;

            $ctrl.propertyName = 'updatedAt';
            $ctrl.reverse = true;

            $ctrl.sortBy = function (propertyName) {
                $ctrl.reverse = ($ctrl.propertyName === propertyName) ? !$ctrl.reverse : false;
                $ctrl.propertyName = propertyName;
            };
        }
    ]
});
