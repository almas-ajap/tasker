angular.module('tasker').directive('toDate', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return new Date(value);
            });
            ngModel.$formatters.push(function (value) {
                return new Date(new Date(value).toLocaleString());
            });
        }
    };
});