var taskerApp = angular.module("tasker", ["ngRoute", "ngCookies"]);

taskerApp.config([
    '$routeProvider',
    '$httpProvider',
    function config(
        $routeProvider,
        $httpProvider
    ) {
        $routeProvider.when('/users', {
            template: '<users></users>'
        }).when('/users/:id', {
            template: '<user></user>'
        }).when('/projects', {
            template: '<projects></projects>'
        }).when('/projects/:id', {
            template: '<project></project>'
        }).when('/todos', {
            template: '<to-dos></to-dos>'
        }).when('/todos/:id', {
            template: '<to-do></to-do>'
        }).otherwise('/users');

        // $httpProvider.interceptors.push(['$q', '$location', '$cookies',
        //     function ($q, $location, $cookies) {
        //         return {
        //             'responseError': function (rejection) {
        //                 if (rejection.status == 403) {
        //                     console.error(rejection);
        //                     $cookies.remove('user');
        //                     $location.path('/login');
        //                 }
        //                 return $q.reject(rejection);
        //             }
        //         };
        //     }
        // ]);
    }
]);

taskerApp.run([
    '$http',
    '$cookies',
    function (
        $http,
        $cookies
    ) {
        // var user = $cookies.getObject('user');
        // console.log(user);
        // if (user) {
        //     $http.defaults.headers.common.Authorization = 'Bearer ' + user.token;
        // }
    }
]);


