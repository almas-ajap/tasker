angular.module('tasker').component('project', {
    templateUrl: 'projects/project/project.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        '$routeParams',
        function (
            $scope,
            $http,
            $cookies,
            $location,
            $routeParams
        ) {
            $scope.id = $routeParams.id;

            $scope.update = function (project) {
                $http.post('/projects/' + $scope.id, project).then(
                    function (res) {
                        console.log(res);
                        $scope.errs = null;
                        $location.path('/projects');
                    },
                    function (errRes) {
                        console.error(errRes);
                        $scope.errs = errRes.data.errors;
                    }
                );
            }

            $scope.get = function () {
                $http.get('/projects/' + $scope.id).then(
                    function (res) {
                        console.log(res);
                        $scope.project = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getAllUsers = function () {
                $http.get('/users').then(
                    function (res) {
                        console.log(res);
                        $scope.users = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            this.$onInit = function () {
                $scope.get();
                $scope.getAllUsers();
            }
        }
    ]
});