angular.module('tasker').component('projects', {
    templateUrl: 'projects/projects.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        function (
            $scope,
            $http,
            $cookies,
            $location
        ) {
            $scope.addNew = function () {
                $http.post('/projects', { name: "New project", description: 'Description for a new project.' }).then(
                    function (res) {
                        console.log(res);
                        $location.path('/projects/' + res.data.id);
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getAll = function () {
                $http.get('/projects').then(
                    function (res) {
                        console.log(res);
                        $scope.projects = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.delete = function (id) {
                $http.delete('/projects/' + id).then(
                    function (res) {
                        console.log(res);
                        $scope.getAll();
                    },
                    function (resErr) {
                        console.error(resErr);
                    }
                );
            }

            this.$onInit = function () {
                $scope.getAll();
            }
        }
    ]
});
