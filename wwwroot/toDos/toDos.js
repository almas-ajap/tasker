angular.module('tasker').component('toDos', {
    templateUrl: 'toDos/toDos.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        '$filter',
        function (
            $scope,
            $http,
            $cookies,
            $location,
            $filter
        ) {
            $scope.addNew = function () {
                $http.post('/toDos', {
                    name: "New toDo",
                    description: 'Description for a new toDo.',
                    startAt: new Date(Date.now())
                }).then(
                    function (res) {
                        console.log(res);
                        $location.path('/todos/' + res.data.id);
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getTodosComplete = function () {
                var count = 0;

                angular.forEach($scope.toDos, function (toDo) {
                    if (toDo.lastState) {
                        if (toDo.lastState.info == "Complete" &&
                            new Date(toDo.lastState.createdAt).between(Date.monday(), Date.next().monday())
                        ) {
                            count++;
                        }
                    }
                });
                $scope.todosComplete = count;
            }

            $scope.getTodosReworking = function () {
                var count = 0;

                angular.forEach($scope.toDos, function (toDo) {
                    if (toDo.lastState && toDo.previousState) {
                        if (toDo.lastState.info == "Working" &&
                            toDo.previousState.info == "Complete" &&
                            new Date(toDo.previousState.createdAt).between(Date.monday(), Date.next().monday())
                        ) {
                            count++;
                        }
                    }
                });
                $scope.todosReworking = count;
            }

            $scope.getAll = function () {
                $http.get('/toDos').then(
                    function (res) {
                        console.log(res);
                        $scope.toDos = [];

                        angular.forEach(res.data, function (v) {
                            if (v.states.length >= 1) {
                                v.lastState = $filter('orderBy')(v.states, true)[0];
                                if (v.states.length >= 2) {
                                    v.previousState = $filter('orderBy')(v.states, true)[1];
                                }
                            }
                            $scope.toDos.push(v);
                        });

                        console.log("todos:", $scope.toDos);

                        $scope.getTodosComplete();
                        $scope.getTodosReworking();
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.delete = function (id) {
                $http.delete('/toDos/' + id).then(
                    function (res) {
                        console.log(res);
                        $scope.getAll();
                    },
                    function (resErr) {
                        console.error(resErr);
                    }
                );
            }

            this.$onInit = function () {
                $scope.getAll();
            }
        }
    ]
});
