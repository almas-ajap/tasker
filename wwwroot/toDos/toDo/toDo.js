angular.module('tasker').component('toDo', {
    templateUrl: 'toDos/toDo/toDo.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        '$routeParams',
        function (
            $scope,
            $http,
            $cookies,
            $location,
            $routeParams
        ) {
            $scope.id = $routeParams.id;

            $scope.update = function (toDo) {
                console.log(toDo);
                $http.post('/toDos/' + $scope.id, toDo).then(
                    function (res) {
                        console.log(res);
                        $scope.errs = null;
                        $location.path('/todos');
                    },
                    function (errRes) {
                        console.error(errRes);
                        $scope.errs = errRes.data.errors;
                    }
                );
            }

            
            $scope.get = function () {
                $http.get('/toDos/' + $scope.id).then(
                    function (res) {
                        console.log(res);
                        $scope.toDo = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getAllUsers = function () {
                $http.get('/users').then(
                    function (res) {
                        console.log(res);
                        $scope.users = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getAllProjects = function () {
                $http.get('/projects').then(
                    function (res) {
                        console.log(res);
                        $scope.projects = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.addState = function(state){
                $http.post('/states', state).then(
                    function (res) {
                        console.log(res);
                        $scope.get();
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            this.$onInit = function () {
                $scope.get();
                $scope.getAllUsers();
                $scope.getAllProjects();
            }
        }
    ]
});