angular.module('tasker').component('users', {
    templateUrl: 'users/users.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        'uniqueStringGenerator',
        function (
            $scope,
            $http,
            $cookies,
            $location,
            uniqueStringGenerator
        ) {
            $scope.addNew = function () {
                var email = uniqueStringGenerator.generate(11) + '@' + uniqueStringGenerator.generate(8) + '.com';
                $http.post('/users', { email: email, password: 'Re2236##' + uniqueStringGenerator.generate(5), role: 'User' }).then(
                    function (res) {
                        console.log(res);
                        $location.path('/users/' + res.data.id);
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.getAll = function () {
                $http.get('/users').then(
                    function (res) {
                        console.log(res);
                        $scope.users = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            $scope.delete = function (id) {
                $http.delete('/users/' + id).then(
                    function (res) {
                        console.log(res);
                        $scope.getAll();
                    },
                    function (resErr) {
                        console.error(resErr);
                    }
                );
            }

            this.$onInit = function () {
                $scope.getAll();
            }
        }
    ]
});
