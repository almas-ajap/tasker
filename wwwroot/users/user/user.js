angular.module('tasker').component('user', {
    templateUrl: 'users/user/user.html',
    controller: [
        '$scope',
        '$http',
        '$cookies',
        '$location',
        '$routeParams',
        function (
            $scope,
            $http,
            $cookies,
            $location,
            $routeParams
        ) {
            $scope.id = $routeParams.id;

            $scope.update = function (user) {
                $http.post('/users/' + $routeParams.id, user).then(
                    function (res) {
                        console.log(res);
                        $scope.errs = null;
                        $location.path('/users');
                    },
                    function (errRes) {
                        console.error(errRes);
                        $scope.errs = errRes.data.errors;
                    }
                );
            }

            $scope.get = function () {
                $http.get('/users/' + $scope.id).then(
                    function (res) {
                        console.log(res);
                        $scope.user = res.data;
                    },
                    function (errRes) {
                        console.error(errRes);
                    }
                );
            }

            this.$onInit = function () {
                $scope.get();
            }
        }
    ]
});