
taskerApp.component('login', {
    templateUrl: 'login.html',
    controller: [
        '$http',
        '$cookies',
        '$location',
        function (
            $http,
            $cookies,
            $location
        ) {
            $ctrl = this;
            $ctrl.credentials = {
                Role: 'User'
            }

            $ctrl.submit = function () {
                $http.post("/users", $ctrl.credentials).then(
                    function (success) {
                        console.log(success);
                        $cookies.putObject('user', success.data);
                        console.log('user in cookies', $cookies.getObject('user'));
                        $http.defaults.headers.common.Authorization = 'Bearer ' + success.data.token;
                        $location.path('/projects');
                    },
                    function (error) {
                        console.error(error);
                    }
                );
            }

            $ctrl.$onInit = function () {

            }
        }
    ]
});
